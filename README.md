# Dependencies

JDK version 8

# How to execute project

Stand in root directory.

Run command:
`./BuildScript.sh`

# Misc

The script takes in a .txt file as data for the main.java file. Modify script
or data.txt for using other data for the program. 