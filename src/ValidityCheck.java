package src;
import java.time.LocalDate;

public class ValidityCheck {

	static boolean isNumberValid(String personalNumber) {
		
		if(!(personalNumber.length() > 9 && personalNumber.length() < 14)){
			System.out.println("Number is in incorrect format");
			return false;
		}
		
		String validLastDig = luhnDigit(personalNumber);
		boolean lastDigMatches = doesLastDigMatch(personalNumber, validLastDig);
		boolean personOverHundred = isPersonOverHundred(personalNumber);
		boolean organizationNumber = isOrganizationNumber(personalNumber);
		if(organizationNumber)
			personOverHundred = false;
		boolean coordinationNumber = isCoordinationNumber(personalNumber);

		if(lastDigMatches){
			System.out.println("\n This is a valid personal number: " + personalNumber +
								"\n Person is over 100 years: " + personOverHundred + 
								"\n Is a coordination number: " + coordinationNumber +
								"\n Is an organization: " + organizationNumber);
			return true;
		}
		else{
			System.out.println("\n This is not a valid personal number: " + personalNumber +
								"\n Luhn digit " + validLastDig +
								" does not match with end digit " + personalNumber.substring(personalNumber.length() - 1));
			return false ;
		}				
	}



	static boolean isPersonOverHundred(String personalNumber){

		//Checks for '+' sign in number
		if(personalNumber.contains("+")){
			return true;
		}

		//Checks first 4 digits for if date difference > 99
		if( personalNumber.length() > 11 && personalNumber.length() < 14){
			LocalDate currenDate = LocalDate.now();
			int currentYear = currenDate.getYear();
			String PNYear = personalNumber.substring(0,4);
			int personYear = Integer.valueOf(PNYear);

			if(currentYear - personYear > 99 )
				return true;
		
		}
		return false;

	}

	// Method for checking if the last digit of the personaln number 
	// checks out with the last digit from the Luhn algorithm
	static boolean doesLastDigMatch(String personalNumber, String lastDig){
		String persNumbLastDig = personalNumber.substring(personalNumber.length() - 1);
		if(persNumbLastDig.equals(lastDig))
			return true;

		return false;
	}

	// Method for checking if the number is a coordination number (samordningsnummer)
	static boolean isCoordinationNumber(String personalNumber){
		if(personalNumber.length() > 11 && personalNumber.length() < 14){
			personalNumber = personalNumber.substring(6, 8);
			int coordNumber = Integer.valueOf(personalNumber);
			if(coordNumber > 60 && coordNumber < 92)
				return true;
		}
		if(personalNumber.length() > 9 && personalNumber.length() < 12){
			personalNumber = personalNumber.substring(4, 6);
			int coordNumber = Integer.valueOf(personalNumber);
			if(coordNumber > 60 && coordNumber < 92)
				return true;
		}
		return false;
	}

	static boolean isOrganizationNumber(String personalNumber){
		if(personalNumber.length() > 11 && personalNumber.length() < 14){
			personalNumber = personalNumber.substring(0, 2);
			int orgNumber = Integer.valueOf(personalNumber);
			if(orgNumber == 16)
				return true;
		}
		if(personalNumber.length() > 9 && personalNumber.length() < 12){
			personalNumber = personalNumber.substring(2, 4);
			int orgNumber = Integer.valueOf(personalNumber);
			if(orgNumber >= 20)
				return true;
		}
		return false;
	}


	// Returns last correct digit
	// for personal number
	static String luhnDigit(String personalNumber) {
		
		// Preprossesing of personal number String
		if(personalNumber.length() > 11 && personalNumber.length() < 14)
			personalNumber = personalNumber.substring(2, personalNumber.length());
		if(personalNumber.contains("+"))
			personalNumber = personalNumber.replace("+", "");
		if(personalNumber.contains("-"))
			personalNumber = personalNumber.replace("-", "");

		if(isBirthdayOnGapYearDay(personalNumber))
			personalNumber = gapYearStringBuilder(personalNumber);

		personalNumber = removeLastChar(personalNumber);


		int nDigits = personalNumber.length();
		int numberSum = 0;
		boolean oddNumber = true;
		
		for (int i = 0; i < nDigits; i++) {
			
			// Converts from char to integer
			int singleDig = personalNumber.charAt(i) - '0';

			if (oddNumber == true)
				singleDig = singleDig * 2;

			// Adding the digit sum
			// followed by modulo 10
			numberSum += singleDig / 10;
			numberSum += singleDig % 10;

			oddNumber = !oddNumber;
		}
		int lastDigit = ((10 - (numberSum % 10)) % 10);

		//Converts to string for later comparison
		String lastDig = String.valueOf(lastDigit);
		return lastDig;
	}

	// Below follows help functions for Luhns Algorithm method
	// -
	// -
	// -

	// Method for removing last char for Luhn algorithm, 
	// handles null exception as well.
	public static String removeLastChar(String personalNumber) {
		return (personalNumber == null || personalNumber.length() == 0)
		  ? null 
		  : (personalNumber.substring(0, personalNumber.length() - 1));
	}

	//Method for checking gap year date of Feb 29
	public static boolean isBirthdayOnGapYearDay(String personalNumber){
		if(personalNumber.length() > 11 && personalNumber.length() < 14){
			personalNumber = personalNumber.substring(4, 8);
			if(personalNumber.equals("0229"))
				return true;
		}
		else{
			personalNumber = personalNumber.substring(2, 6);
			if(personalNumber.equals("0229"))
				return true;
		}
		return false;
	}

	// Method for changing gap year date to march 01 which is the legal birthday
	// in this edge case
	public static String gapYearStringBuilder(String personalNumber){
		String correctDate = "0301";
			personalNumber = personalNumber.substring(0, 2)
							 + correctDate 
							 + personalNumber.substring(6, personalNumber.length());
			return personalNumber;	
	}
}
