package src;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static ArrayList<String> readData(String pathName) {
    ArrayList<String> numbers = new ArrayList<>();

    // we create a scanner for reading the data file
    try (Scanner scanner = new Scanner(Paths.get(pathName))) {

        // we read all the lines of the file
        while (scanner.hasNextLine()) {
            numbers.add(scanner.nextLine());
        }
    } catch (Exception e) {
        System.out.println("Error: " + e.getMessage());
    }

    return numbers;

    }



    	// Driver code
	public static void main(String[] args) {

        String pathName = String.valueOf(args[0]);
		ArrayList<String> numbers = readData(pathName);

        int validCount = 0;
        int nonValidCount = 0;

        for(int i = 0 ; i < numbers.size() ; i++) {

            String personalNumber = numbers.get(i);
            
            boolean validNumber = ValidityCheck.isNumberValid(personalNumber);

            if(validNumber)
                validCount++;
            else{
                nonValidCount++;
                }
            }
        
        System.out.println("\n Number of valid personal numbers: " + validCount
                            + "\n Number of invalid personal numbers: " + nonValidCount);
    }      
}
