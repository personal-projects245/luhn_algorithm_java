
# Compile Java program
javac src/Main.java

# Check if compilation was successful
if [ $? -eq 0 ]; then
    echo "Compilation successful."

    # Run Java program with input from data.txt
    java src/Main data.txt
else
    echo "Compilation failed. Please fix errors before running the program."
fi
